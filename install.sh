#! /bin/bash

timedatectl set-ntp true

parted -s /dev/vda mklabel msdos
parted -s /dev/vda mkpart primary ext4 0% 100%
parted -s /dev/vda set 1 boot on

mkfs.ext4 -F /dev/vda1
mount /dev/vda1 /mnt

pacstrap /mnt base base-devel

genfstab -U /mnt >> /mnt/etc/fstab
arch-chroot /mnt /bin/bash

echo en_US.UTF-8 UTF-8 >> /etc/locale.gen
locale-gen
echo LANG=en_US.UTF-8 >> /etc/locale.conf

ln -s /usr/share/zoneinfo/Europe/Amsterdam /etc/localtime
hwclock --systohc --utc

mkinitcpio -p linux

pacman --noconfirm -S grub
grub-install /dev/vda
grub-mkconfig -o /boot/grub/grub.cfg

echo hostname > /etc/hostname
systemctl enable dhcpcd.service

exit
umount /mnt
shutdown now
